QwikTweet
=========

QwikTweet is a tool for access control and community automation of the Twitter account.

It performs two functions:

-Provide restricted user access to a Twitter account
-Automate the publication of posts (drawn from an RSS feed) on Twitter.

========================
INSTALL

1. Upload the contents of /QwikTweet/ to your server.
2. Create the database by executing /install/createdb.sql.
3. Create a database user and add the details to config.php.
4. If you haven't already, create a Twitter application on the account you wish to QwikTweet to integrate with. 
   Copy the OAuth tokens into config.php.
