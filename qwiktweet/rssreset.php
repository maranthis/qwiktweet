<?php


session_start();
require_once("config.php");

if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}

	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);

	
	if (isset($_POST["feed"])) {
		
		$sql = "SELECT * FROM rssfeeds WHERE idfeed = :idfeed";
		$q = $conn->prepare($sql);
		$q->bindParam(":idfeed", $_POST["feed"]);
		$q->execute();
		foreach ($q as $row) {
			$idfeed = $row["idfeed"];
			$prefix = $row["prefix"];
			$suffix = $row["suffix"];
			$name = $row["name"];
			$url = $row["url"];
			
			$sql = "SELECT lastguid FROM rssfeeds WHERE idfeed = :idfeed";
			$q = $conn->prepare($sql);	
			$q->bindParam(":idfeed", $idfeed);
			$q->execute();
			$guid = $q->fetchColumn(0);	
			
			$doc = new DOMDocument();
			$doc->load($url);
			$arrFeeds = array();
			
			foreach ($doc->getElementsByTagName('item') as $node) {
				$itemRSS = array ( 
					'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
					'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
					'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
					'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
					'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue
				);
				array_push($arrFeeds, $itemRSS);
			}	  
		
			$i = 0;	
			foreach ($arrFeeds as $entry) {
				if (($guid != $entry['guid']) && ($i < 3)) {
					$tweet = substr($entry['title'],0,100) . ': ';
					//$url = ssfURLShorten($entry['link']);
					$strbuilt = $prefix . ' ' . $tweet . '(short URL not regenerated)' . $suffix;
					echo htmlspecialchars($strbuilt) . '<br />';
					$i++;
				}
				else {
					break;
				}
			}	
			if ($i != 0) {
				$sql = "UPDATE rssfeeds SET lastguid = :lastguid WHERE idfeed = :idfeed";
				$q = $conn->prepare($sql);
				$q->bindParam(":lastguid", $arrFeeds[0]['guid']);
				$q->bindParam(":idfeed", $idfeed);
				$q->execute();
				$message = 'QwikTweet resynced for idfeed = ' . $name . '.';
				$sql = "INSERT INTO tlog (tweet, user) VALUES (:logmessage, 'syslog')";
				$q = $conn->prepare($sql);
				$logmessage = $i . " tweet(s) suppressed for " . $name;
				$q->bindParam(":logmessage", $logmessage);
				$q->execute();
			}
			else {
				$message = 'No new posts found on ' . $name . '.';
				$sql = "INSERT INTO tlog (tweet, user) VALUES (:logmessage, 'syslog')";
				$logmessage = $name . " tweet suppress called, but no new tweets.";
				$q = $conn->prepare($sql);
				$q->bindParam(":logmessage", $logmessage);
				$q->execute();
			}
		}
	}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
      <img src="qwiktweet.png" />
        <p><b>Feed Synchronise/Reset</b></p>
        <center>
        <form id="rssreset" name="rssreset" method="post" action="rssreset.php">
  <p style="line-height: 30px;">
    <label for="service">Feed</label>
    <select name="feed" id="feed">
            <?php
		$sql = "SELECT * FROM rssfeeds where enabled = 1";
		foreach ($conn->query($sql) as $row) {
			echo("<option value=\"" . (int) $row["idfeed"] . "\">" . htmlspecialchars($row["name"]) . "</option>");
		}
		?>
    </select>
    <br />
<input type="submit" name="Submit" id="Submit" value="Submit" />
  </p>
  <p><?php if (isset($message)) { echo htmlspecialchars($message) . "<br />"; } ?><a href="tweet.php">Back</a><br />
</p>
</form>

<p style="font-size: 11px"><b>Current QwikTweet queue for Soshified:</b></p>
<span class="tweet-text">
<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');	
	require_once('config.php');
	require_once('EpiCurl.php');
    require_once('EpiOAuth.php');
	require_once('EpiTwitter.php');
	

	function tweet($tweet) {
		global $consumer_key, $consumer_secret, $token, $secret;
		$twitterObj = new EpiTwitter($consumer_key, $consumer_secret, $token, $secret);
		$status = $twitterObj->post('/statuses/update.json', array('status' => stripslashes($tweet)));
	}	
		  
	function ssfURLShorten($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://ssf.me/bookmark?key=d41d8cd98f00b204e9800998ecf8427e&url=" . $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$shorturl = curl_exec($ch);
		curl_close($ch);
		return $shorturl;
	}
	
	function yoonaURLShorten($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://yoona.im/yourls-api.php?username=api&password=f35453685ad77fc8a4b7ae4be770a206&action=shorturl&format=simple&url=" . $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$shorturl = curl_exec($ch);
		curl_close($ch);
		return $shorturl;
	}
	
	require_once('EpiCurl.php');
	require_once('EpiOAuth.php');
	require_once('EpiTwitter.php');
	
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$sql = "SELECT * FROM rssfeeds WHERE enabled = 1";
	
	foreach ($conn->query($sql) as $row) {
		$idfeed = $row["idfeed"];
		$prefix = $row["prefix"];
		$suffix = $row["suffix"];
		$url = $row["url"];
		
		$sql = "SELECT lastguid FROM rssfeeds WHERE idfeed = :idfeed";
		$q = $conn->prepare($sql);	
		$q->bindParam(":idfeed", $idfeed);
		$q->execute();
		$guid = $q->fetchColumn(0);	
		
		$doc = new DOMDocument();
		$doc->load($url);
		$arrFeeds = array();
		
		foreach ($doc->getElementsByTagName('item') as $node) {
			if ($node->getElementsByTagName('category')->length != 0)
			{
				$itemRSS = array ( 
					'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
					'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
					'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
					'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
					'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue,
					'cat' => $node->getElementsByTagName('category')->item(0)->nodeValue
					);
			}
			else
			{
				$itemRSS = array ( 
					'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
					'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
					'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
					'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
					'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue
					);
			}
			
			array_push($arrFeeds, $itemRSS);
		}	  
	
		$i = 0;	
		foreach ($arrFeeds as $entry) {
			if (($guid != $entry['guid']) && ($i < 3)) {
				//if ($i < 3) {
				$tweet = substr($entry['title'],0,100) . ': ';
				
				if (isset($entry['cat'])) {
					$prefix = "[" . strToUpper($entry['cat']) . "]";
				}				
				$url = ssfURLShorten($entry['link']);
				$strbuilt = $prefix . ' ' . $tweet . $url . ' ' . $suffix;
				echo htmlspecialchars($strbuilt) . '<br />';
				$i++;
			}
			else {
				break;
			}
		}	
		if ($i == 0) {
			echo 'No new tweets on: ' . htmlspecialchars($url) . '<br />';
			
		}
	}
?></p>
        </center>
      </div>
    </div>
</div>

</body>
</html>