<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');	
session_start();
include("config.php");
$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);

if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {
	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$redircode = $q->fetchColumn(0);
	if ($redircode != 1) {
		header("Location: index.php");	
	}	
}


if (isset($_GET["deact"])) {
	$feedid = $_GET["deact"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$isadmin = $q->fetchColumn(0);
	if ($isadmin == 1) {
		$sql = "UPDATE rssfeeds SET enabled = 0 WHERE idfeed = :feedid";
		$q = $conn->prepare($sql);
		$q->bindParam(":feedid", $feedid);
		$q->execute();
	}	
}

if (isset($_GET["act"])) {

	$feedid = $_GET["act"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$isadmin = $q->fetchColumn(0);		
	if ($isadmin == 1) {
		$sql = "UPDATE rssfeeds SET enabled = 1 WHERE idfeed = :feedid";
		$q = $conn->prepare($sql);
		$q->bindParam(":feedid", $feedid);
		$q->execute();
		$sql = "SELECT url FROM rssfeeds WHERE idfeed = :idfeed";
		$q = $conn->prepare($sql);	
		$q->bindParam(":idfeed", $feedid);
		$q->execute();
		$url = $q->fetchColumn(0);
		// need to load last guid in so that we don't get a burst of tweets from the past	
		$doc = new DOMDocument();
		$doc->load($url);
		$arrFeeds = array();	
		foreach ($doc->getElementsByTagName('item') as $node) {
			$itemRSS = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
				'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue
			);
			array_push($arrFeeds, $itemRSS);
		}
		$sql = 'UPDATE rssfeeds SET lastguid = :lastguid WHERE idfeed = :feedid';
		$q = $conn->prepare($sql);
		$q->bindParam(":lastguid", $arrFeeds[0]['guid']);
		$q->bindParam(":feedid", $feedid);
		$q->execute();
	}
}
	
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - RSS Admin</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" /><b>RSS Administration</b>
        <center>
        <p><a href="rss-create.php">Add Feed</a></p>
        Feed List<br />        
        <table>
        <?php
		$sql = "SELECT * FROM rssfeeds";
		foreach ($conn->query($sql) as $row) {
			echo("<tr><td>" . htmlspecialchars($row["name"]) . "</td>");
			//echo("<td><a href=\"rss-edit.php?id=" . $row["idfeed"] . "\">Edit</a></td>");
			echo("<td></td>");

			if ($row["enabled"] == 0) {
				echo("<td><a href=\"rssadmin.php?act=" . (int) $row["idfeed"] . "\">Activate</a></td><tr>");
			}
			else {
				echo("<td><a href=\"rssadmin.php?deact=" . (int) $row["idfeed"] . "\">Deactivate</a></td><tr>");
			}
			
		}
		?>
        </table>
        <br />
        <a href="tweet.php">Back</a>
        
        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>