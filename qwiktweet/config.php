<?php

require_once('EpiCurl.php');
require_once('EpiOAuth.php');
require_once('EpiTwitter.php');

// Local DB details
$dbhost     = "localhost";
$dbname     = "qwiktweet";
$dbuser     = "qwiktweet";
$dbpass     = "YOUR_PASSWORD_HERE";

// Twitter details
$consumer_key = 'CONSUMER_KEY';
$consumer_secret = 'CONSUMER_SECRET';
$token = 'TOKEN';
$secret= 'SECRET';

?>