<?php
session_start();
include("config.php");
$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);

if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {

	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$redircode = $q->fetchColumn(0);
	if ($redircode != 1) {
		header("Location: index.php");	
	}
	
}


if (isset($_GET["del"])) {
	$username = $_GET["del"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$isadmin = $q->fetchColumn(0);
	if ($isadmin != 1) {
		$sql = "DELETE FROM users WHERE username = :username";
		$q = $conn->prepare($sql);
		$q->bindParam(":username", $username);
		$q->execute();
	}	
	$deleted = true;	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - Create User</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" /><b>User Administration</b>
        <center>
        <?php
			if ($deleted) {
				echo ("<br />User deleted.<br /><br />");
			}
		?>
        <p><a href="createuser.php">Create User</a></p>
        User List<br />        
        <table>
        <?php
		$sql = "SELECT username, admin FROM users";
		foreach ($conn->query($sql) as $row) {
			echo("<tr><td>" . htmlspecialchars($row["username"]) . "</td>");
			if (($row["username"] == $_SESSION["un"]) || ($row["admin"] == 0)) {
				echo("<td><a href=\"changepw.php?usr=" . urlencode($row["username"]) . "\">Change PW</a></td>");
			}
			else {
				echo("<td></td>");
			}
			if ($row["admin"] == 0) {
				echo("<td><a href=\"useradmin.php?del=" . urlencode($row["username"]) . "\">Delete</a></td><tr>");
			}
			else {
				echo("<td></td>");
			}
			
		}
		?>
        </table>
        <br />
        <a href="tweet.php">Back</a>
        
        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>