<?php
session_start();
include("config.php");
if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {
	//Check if user is not self
	if ($_GET["usr"] != $_SESSION["un"]) {
		//if user is not self check whether user is an admin
		$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
		$username = $_SESSION["un"];
		$sql = "SELECT admin FROM users WHERE username = :username";
		$q = $conn->prepare($sql);	
		$q->bindParam(":username", $username);
		$q->execute();
		$redircode = $q->fetchColumn(0);
		if ($redircode != 1) {
			//kick user out
			header("Location: index.php");	
		}
	}	
}
if (isset($_POST["password"])) {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_POST["username"];
	$password = md5($_POST["password"]);
	$sql = "UPDATE users SET password = :password WHERE username = :usr";
	$q = $conn->prepare($sql);
	$hashedpassword = md5($_POST["password"]);
	$q->bindParam(":password", $hashedpassword);
	$q->bindParam(":usr", $_GET["usr"]);
	$q->execute();
	$changed = true;	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - Change Password</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" /><b>Change Password</b>
        <center>
        <?php
			if ($changed) {
				echo ("<br />Password changed.<br />");
			}
		?>
        <br />
        <form action="changepw.php?usr=<?php echo (urlencode($_GET["usr"])); ?>" method="post" target="_self">
            <table>
                    <tr>
                        <td>Username:</td><td><?php echo (htmlspecialchars($_GET["usr"])); ?></td>
                    </tr>
              <tr>
                <td> Password:</td><td><input name="password" type="password" /></td>
              </tr>
          </table><input name="Submit" type="submit" value="Submit" />
        </form>
        <br />
        <a href="tweet.php">Back</a>

        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>