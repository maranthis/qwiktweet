<?php
session_start();
include("config.php");

if (isset($_POST["username"])) {

	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_POST["username"];
	$password = $_POST["password"];
	$sql = "SELECT username FROM users WHERE username = :username AND password = :password";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->bindParam(":password", md5($_POST["password"]));
	$q->execute();
	
	$redircode = $q->fetchColumn(0);
	//echo $redircode;
	if ($redircode) {
		$sql = "INSERT INTO tlog (tweet, user) VALUES ('EVENT: User logged in', :user)";
		$q = $conn->prepare($sql);
		$q->bindParam(":user", $username);
		$q->execute();
		$_SESSION["un"] = $username;
		header("Location: tweet.php");	
	}
	else {
		
		$sql = "INSERT INTO tlog (tweet, user) VALUES ('EVENT: User failed login', :creds)";
		$q = $conn->prepare($sql);
		$creds = $username . " PW:" . $password;
		$q->bindParam(":creds", $creds);
		$q->execute();
	}
}
else {
	unset($_SESSION["un"]);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="startup.png">
<title>QwikTweet</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" />
        <center>
        <form action="index.php" method="post" target="_self">
            <table>
                    <tr>
                        <td>Username:</td><td><input name="username" type="text" /></td>
                    </tr>
              <tr>
                <td> Password:</td><td><input name="password" type="password" /></td>
              </tr>
          </table><input name="Submit" type="submit" value="Submit" />
        </form>
        <br />
        <span class="smalltext"><b>warning:</b><br />usage of QwikTweet (including unauthorised login attempts)<br />is monitored.<br /></span>
        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>