<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');	

session_start();
include("config.php");
if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else { /*
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$redircode = $q->fetchColumn(0);
	if ($redircode != 1) {
		header("Location: index.php");	*/
}

if (isset($_GET["del"])) {
	$twitterObj = new EpiTwitter($consumer_key, $consumer_secret, $token, $secret);
	$id = number_format($_GET["del"], 0, '.', '');
	$jsonurl = "/statuses/destroy/" . urlencode($id) . ".json";
	$status = $twitterObj->post($jsonurl);
}
else {
	header("Location: tweet.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - Delete Tweet</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
      <center>
        <img src="qwiktweet.png" />
        <table width="280">
        <tr><td>     	
        Tweet (ID: <?php echo rtrim(rtrim(sprintf('%.20F', $id), '0'), '.') ?>) deleted. <br />
        Please note that it might take up to 30 seconds for this to be reflected in the user interface.       
        </td></tr>
        </table>
         <a href="tweet.php">Back</a>
        <br /><br />
        </center>
      </div>
    </div>
</div>

</body>
</html>