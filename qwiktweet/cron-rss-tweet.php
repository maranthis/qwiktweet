<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');	
	require_once('config.php');
	require_once('EpiCurl.php');
    require_once('EpiOAuth.php');
	require_once('EpiTwitter.php');
	

	function tweet($tweet) {
		global $consumer_key, $consumer_secret, $token, $secret;
		$twitterObj = new EpiTwitter($consumer_key, $consumer_secret, $token, $secret);
		$status = $twitterObj->post('/statuses/update.json', array('status' => stripslashes($tweet)));
	}	
		  
	function ssfURLShorten($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://YOUR_SHORTENER_URL/bookmark?key=YOUR_SHORTENER_API_KEY&url=" . $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$shorturl = curl_exec($ch);
		curl_close($ch);
		return $shorturl;
	}
	

	function ggReplace($tweet) {
		$markers = array("Girls' Generation", "Girl's Generation", "Girls Generation", "SNSD");
		$replaced = str_replace($markers, "@GirlsGeneration", $tweet);
		return $replaced;
	}
	
	require_once('EpiCurl.php');
	require_once('EpiOAuth.php');
	require_once('EpiTwitter.php');
	
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$sql = "SELECT * FROM rssfeeds WHERE enabled = 1";
	
	foreach ($conn->query($sql) as $row) {
		$idfeed = $row["idfeed"];
		$prefix = $row["prefix"];
		$suffix = $row["suffix"];
		$url = $row["url"];
		
		$sql = "SELECT lastguid FROM rssfeeds WHERE idfeed = :idfeed";
		$q = $conn->prepare($sql);	
		$q->bindParam(":idfeed", $idfeed);
		$q->execute();
		$guid = $q->fetchColumn(0);	
		
		$doc = new DOMDocument();
		$doc->load($url);
		$arrFeeds = array();
		
		foreach ($doc->getElementsByTagName('item') as $node) {
			if ($node->getElementsByTagName('category')->length != 0)
			{
				$itemRSS = array ( 
					'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
					'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
					'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
					'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
					'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue,
					'cat' => $node->getElementsByTagName('category')->item(0)->nodeValue
					);
			}
			else
			{
				$itemRSS = array ( 
					'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
					'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
					'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
					'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
					'guid' => $node->getElementsByTagName('guid')->item(0)->nodeValue
					);
			}
			array_push($arrFeeds, $itemRSS);
		}	  
	
		$i = 0;	
		foreach ($arrFeeds as $entry) {
			if (($guid != $entry['guid']) && ($i < 3)) {
				$tweet = html_entity_decode(substr($entry['title'],0,120), ENT_COMPAT, "UTF-8") . ': ';
				if (isset($entry['cat']) && ($idfeed == 9)) {
					if ($entry['cat'] == "FEATURES") {
						$prefix = "[FEATURE]";
					}
					else {
						$prefix = "[" . strToUpper($entry['cat']) . "]";
					}
					$tweet = ggReplace($tweet);
				}
				$url = ssfURLShorten($entry['link']);
				
				//$url = $entry['link'];
				$strbuilt = $prefix . ' ' . $tweet . $url . ' ' . $suffix;
				echo htmlspecialchars($strbuilt) . '<br />';
				if ($url != "") {
					tweet($strbuilt);
					$sql = "INSERT INTO tlog (tweet, user, url, lastguid, idfeed, guid) VALUES (:strbuilt, 'syslog', :url, :lastguid, :idfeed, :entryguid)";
					$q = $conn->prepare($sql);
					$q->bindParam(":strbuilt", $strbuilt);
					$q->bindParam(":url", $url);
					$q->bindParam(":lastguid", $arrFeeds[0]['guid']);
					$q->bindParam(":idfeed", $idfeed);
					$q->bindParam(":entryguid", $entry['guid']);
					$q->execute();
					$i++;
				}
				else {
					$sql = "INSERT INTO tlog (tweet, user, url, lastguid, idfeed, guid) VALUES (:strbuilt, 'syslog', 'ERROR: blankurl', :lastguid, :idfeed, :entryguid)";
					$q = $conn->prepare($sql);
					$q->bindParam(":strbuilt", $strbuilt);
					$q->bindParam(":lastguid", $arrFeeds[0]['guid']);
					$q->bindParam(":idfeed", $idfeed);
					$q->bindParam(":entryguid", $entry['guid']);
					$q->execute();
				}				
			}
			else {
				break;
			}
		}	
		if ($i != 0) {
			$sql = "UPDATE rssfeeds SET lastguid = :lastguid WHERE idfeed = :idfeed";
			$q = $conn->prepare($sql);
			$q->bindParam(":lastguid", $arrFeeds[0]['guid']);
			$q->bindParam(":idfeed", $idfeed);
			$q->execute();
		}
		else {
			$sql = "INSERT INTO tlog (tweet, user) VALUES (:url, 'syslog')";
			$q = $conn->prepare($sql);
			$concattext = "No new tweets on: " . $url;
			$q->bindParam(":url", $concattext);
			$q->execute();
			echo 'No new tweets on: ' . htmlspecialchars($url) . '<br />';			
		}
	}
?>
