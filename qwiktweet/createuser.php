<?php
session_start();
include("config.php");
if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$redircode = $q->fetchColumn(0);
	if ($redircode != 1) {
		header("Location: index.php");	
	}
	
}


if (isset($_POST["username"])) {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_POST["username"];
	$password = md5($_POST["password"]);
	$sql = "INSERT INTO users VALUES(:username, :password, ' ', '0')";
	$q = $conn->prepare($sql);
	$q->bindParam(":username", $username);
	$q->bindParam(":password", $password);
	$q->execute();
	$created = true;	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - Create User</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" /><b>Create User</b>
        <center>
        <?php
			if ($created) {
				echo ("<br />User created.<br />");
			}
		?>
        <form action="createuser.php" method="post" target="_self">
            <table>
                    <tr>
                        <td>Username:</td><td><input name="username" type="text" autocomplete="off" /></td>
                    </tr>
              <tr>
                <td> Password:</td><td><input name="password" type="password" autocomplete="off"/></td>
              </tr>
          </table><input name="Submit" type="submit" value="Submit" />
        </form>
        <br />
        <a href="tweet.php">Back</a>

        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>