<?php
session_start();
include("config.php");
if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$redircode = $q->fetchColumn(0);
	if ($redircode != 1) {
		header("Location: index.php");	
	}
	
}


if (isset($_POST["name"])) {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$name = $_POST["name"];
	$prefix = $_POST["prefix"];
	$url = $_POST["url"];
	$suffix = $_POST["suffix"];
	$sql = "INSERT INTO rssfeeds (url, name, prefix, suffix) VALUES(:url, :name, :prefix,  :suffix)";
	$q = $conn->prepare($sql);
	$q->bindParam(":url", $url);
	$q->bindParam(":name", $name);
	$q->bindParam(":prefix", $prefix);
	$q->bindParam(":suffix", $suffix);
	$q->execute();
	$created = true;	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet - Add Feed</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" /><b>Add Feed</b>
        <center>
        <?php
			if ($created) {
				echo ("<br />Feed created.<br />");
			}
		?>
        <form action="rss-create.php" method="post" target="_self">
            <table>
                    <tr>
                        <td>Feed Name:</td><td><input name="name" type="text" /></td>
                    </tr>
              <tr>
                <td>Feed URL:</td><td><input name="url" type="text" /></td>
              </tr>
              <tr>
                <td>Tweet Prefix:</td><td><input name="prefix" type="text" /></td>
              </tr>
              <tr>
                <td>Tweet Suffix:</td><td><input name="suffix" type="text" /></td>
              </tr>
          </table><input name="Submit" type="submit" value="Submit" />
        </form>
        <br />
        <a href="rssadmin.php">Back</a>

        </center>
        <br />
      </div>
    </div>
</div>

</body>
</html>