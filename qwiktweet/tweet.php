<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');	

session_start();
require_once("config.php");

$status = false; //setting page state - this is a postback + error check
if (!isset($_SESSION["un"])) {
	header("Location: index.php");
}
else {
	// check whether this person is logged in as administrator (and valid)
	// compare username against admin column in database
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$username = $_SESSION["un"];
	$sql = "SELECT admin FROM users WHERE username = :username";
	$q = $conn->prepare($sql);	
	$q->bindParam(":username", $username);
	$q->execute();
	$admin = $q->fetchColumn(0);
}

// if postback, send tweet
if (isset($_POST["tweet"])) {
	$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
	$twitterObj = new EpiTwitter($consumer_key, $consumer_secret, $token, $secret);
	$status = $twitterObj->post('/statuses/update.json', array('status' => stripslashes($_POST["tweet"])));
	$sql = "INSERT INTO tlog (tweet, user) VALUES (:tweet, :un)";
	$q = $conn->prepare($sql);
	$q->bindParam(":tweet", $_POST["tweet"]);
	$q->bindParam(":un", $_SESSION["un"]);
	$q->execute();
}

// link parser to parse links in the short recent tweets preview
define( 'LINK_LIMIT', 140 );
define( 'LINK_FORMAT', '<a href="%s" rel="ext" target="_blank">%s</a>' );
function parse_links  ( $m )
{
    $href = $name = html_entity_decode($m[0]);

    if ( strpos( $href, '://' ) === false ) {
        $href = 'http://' . $href;
    }

    if( strlen($name) > LINK_LIMIT ) {
        $k = ( LINK_LIMIT - 3 ) >> 1;
        $name = substr( $name, 0, $k ) . '...' . substr( $name, -$k );
    }

    return sprintf( LINK_FORMAT, htmlentities($href), htmlentities($name) );
}
$reg = '~((?:https?://|www\d*\.)\S+[-\w+&@#/%=\~|])~';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-startup-image" href="/startup.png">
<title>QwikTweet</title>
<link href="style.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript">
function charCount(field, targetfield, max) {
	if (field.value.length > max)
		field.value = field.value.substring(0,max);
	else
		targetfield.value = max - field.value.length;
}
</script>
</head>
<body>
<div class="center">
    <div class="shadow">
      <div class="content-body">
        <img src="qwiktweet.png" />
        <center>
        <?php
		
		// display logged in username
		echo("Logged in as " . htmlspecialchars($_SESSION["un"]) . "<br />");
		
		// display tweet sent notification if it happened
        if ($status) {
			echo("<center>Tweet sent.</center>");	
        }
		?>
        <form action="tweet.php" method="post" target="_self">
            <table>
                    <tr>
                        <td><textarea name="tweet" cols="30" rows="4" onKeyUp="charCount(this.form.tweet,this.form.chars,140);" onKeyDown="charCount(this.form.tweet,this.form.chars,140);"></textarea></td>
                    </tr>
                    <tr>
                    <td><center><input readonly name="chars" type="text" size="3" value="140"/> characters remaining. <br /><input name="Submit" type="submit" value="Tweet" /></center></td></tr>
          </table>
          <span class="smalltext">got a url? use the <a target="_blank" href="https://qwiktweet.com/urlshorten.php">super-cool url shorteners</a><br />Easy access to @soshified, just like <a href="img/201103311932041001_1.jpg">a zip.</a></span><br />
          <br />
          <span class="tweet-text">
          <table width="280">
<?php
try {
	// sign into twitter and get timeline
	$twitterObj = new EpiTwitter($consumer_key, $consumer_secret, $token, $secret);
	$timeline = $twitterObj->get('/statuses/user_timeline.json');
	
	// display 4 most recent tweets
	$i = 0;
	foreach($timeline as $tweet) {
		if ($i < 4) {
			$id = number_format($tweet->id, 0, '.', '');
			echo "<tr><td>";
			echo "<span class=\"tweet\">";
			echo preg_replace_callback($reg, 'parse_links', $tweet->text);
			echo"</span>";
			echo "</td></tr>";
			echo "<tr><td>";
			$tweetdate = new DateTime(str_replace(",", "", $tweet->created_at));	
			echo $tweetdate->format("jS F Y h:ia \G\M\T");		
			if (1 == 1) {
				echo " - ";
				echo "<a href=\"deltweet.php?del=" . rtrim(rtrim(sprintf('%.20F', $id), '0'), '.') . "\">Delete</a>";
			}
			echo "</td></tr>";
			echo "<tr></tr>";
		}
		$i++;
	}
}
catch (Exception $ex) {
	echo "Whoops! The Twitter API is either unavailable, or API access has been revoked via the Twitter web interface. Please contact <a href='http://twitter.com/ssfleddy'>Leddy (@ssfleddy)</a> at your earliest convenience to have access restored.";
}

?>
</table></span>
        </form>
        <?php
		// if an administrator, display the admin links

			if ($admin == 1) {
				echo("<p><a href=\"useradmin.php\">User Admin</a> - <a href=\"rssadmin.php\">Feed Admin</a> - <a href=\"rssreset.php\">Feed Sync</a><br /></p>");
			}
			else {
				echo("<p><a href=\"changepw.php?usr=" . urlencode($_SESSION["un"]) . "\">Change my password</a> - <a href=\"rssreset.php\">Feed Sync</a></p>");
			}
		?>
        
        </center>
      </div>
    </div>
</div>

</body>
</html>