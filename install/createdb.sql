SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `qwiktweet` DEFAULT CHARACTER SET latin1 ;
USE `qwiktweet` ;

-- -----------------------------------------------------
-- Table `qwiktweet`.`rssfeeds`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `qwiktweet`.`rssfeeds` (
  `idfeed` INT(11) NOT NULL AUTO_INCREMENT ,
  `url` VARCHAR(300) NOT NULL ,
  `lastguid` VARCHAR(500) NULL DEFAULT NULL ,
  `name` VARCHAR(50) NOT NULL ,
  `enabled` INT(11) NULL DEFAULT '0' ,
  `prefix` VARCHAR(45) NULL DEFAULT NULL ,
  `suffix` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`idfeed`) )
ENGINE = MyISAM
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `qwiktweet`.`tlog`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `qwiktweet`.`tlog` (
  `tid` INT(11) NOT NULL AUTO_INCREMENT ,
  `tweet` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `user` VARCHAR(50) CHARACTER SET 'latin1' NOT NULL ,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `url` VARCHAR(100) CHARACTER SET 'latin1' NULL DEFAULT NULL ,
  `guid` VARCHAR(500) CHARACTER SET 'latin1' NULL DEFAULT NULL ,
  `idfeed` VARCHAR(45) CHARACTER SET 'latin1' NULL DEFAULT NULL ,
  `lastguid` VARCHAR(500) CHARACTER SET 'latin1' NULL DEFAULT NULL ,
  PRIMARY KEY (`tid`) )
ENGINE = MyISAM
AUTO_INCREMENT = 448243
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `qwiktweet`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `qwiktweet`.`users` (
  `username` VARCHAR(50) NOT NULL ,
  `password` VARCHAR(50) NOT NULL ,
  `sessionid` VARCHAR(100) NOT NULL ,
  `admin` INT(11) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`username`) )
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
